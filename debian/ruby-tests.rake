require 'gem2deb/rake/testtask'

exclude = %w[
  test/test_reporter.rb
  test/test_reporter_public_start_stop.rb
  test/test_reporter_private_start_stop.rb
]

Gem2Deb::Rake::TestTask.new do |t|
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb'] - exclude
end
